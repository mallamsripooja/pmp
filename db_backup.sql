-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: purchasemnmgmt
-- ------------------------------------------------------
-- Server version	5.6.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `dealerId` int(11) NOT NULL,
  PRIMARY KEY (`categoryId`),
  KEY `fk_category_1_idx` (`dealerId`)
) ENGINE=InnoDB AUTO_INCREMENT=1506 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1500,'Handbags',100),(1501,'Home decor',100),(1502,'Stationery',101),(1503,'Detergent',101),(1504,'Vegetables',102),(1505,'Chocolates',102);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealer`
--

DROP TABLE IF EXISTS `dealer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealer` (
  `dealerId` int(10) NOT NULL AUTO_INCREMENT,
  `password` varchar(45) NOT NULL,
  `requestId` int(11) NOT NULL,
  PRIMARY KEY (`dealerId`),
  UNIQUE KEY `requestId_UNIQUE` (`requestId`),
  CONSTRAINT `fk_rid` FOREIGN KEY (`requestId`) REFERENCES `dealer_details` (`requestId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealer`
--

LOCK TABLES `dealer` WRITE;
/*!40000 ALTER TABLE `dealer` DISABLE KEYS */;
INSERT INTO `dealer` VALUES (100,'26466',17),(101,'38299',20),(102,'28831',21);
/*!40000 ALTER TABLE `dealer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealer_details`
--

DROP TABLE IF EXISTS `dealer_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealer_details` (
  `requestId` int(11) NOT NULL AUTO_INCREMENT,
  `dealerId` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `area` varchar(200) NOT NULL,
  `email` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `company` varchar(100) NOT NULL,
  PRIMARY KEY (`requestId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealer_details`
--

LOCK TABLES `dealer_details` WRITE;
/*!40000 ALTER TABLE `dealer_details` DISABLE KEYS */;
INSERT INTO `dealer_details` VALUES (17,100,'Sripooja Mallam ','4-1/B, ABC Estates, Road No - 36, Jubilee Hills, Hyderabad, Telangana.','project24.kmit@gmail.com','9898989800','SM Handloom'),(20,101,'Vishal Mehra','Street no-3, Banjara hills, Hyderabad, Telangana.','project24.kmit@gmail.com','9898989811','VM Retailers'),(21,102,'Anand Vihari ','Street no 2, Road no 3, RK Circle, Hyderabad.','project24.kmit@gmail.com','7865432122','Anand Agencies');
/*!40000 ALTER TABLE `dealer_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ex_dealer_details`
--

DROP TABLE IF EXISTS `ex_dealer_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ex_dealer_details` (
  `dealerId` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(45) NOT NULL,
  `area` varchar(200) NOT NULL,
  `email` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `reason` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dealerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ex_dealer_details`
--

LOCK TABLES `ex_dealer_details` WRITE;
/*!40000 ALTER TABLE `ex_dealer_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `ex_dealer_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `transactionId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `dealerId` int(11) NOT NULL,
  PRIMARY KEY (`transactionId`),
  KEY `fk_product` (`productId`),
  KEY `fk_user` (`userId`),
  CONSTRAINT `fk_product` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2014 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (2000,500,1000,1,'2018-03-26','cart',100),(2001,500,1007,1,'2018-03-26','cart',101),(2002,500,1010,1,'2018-03-26','cart',101),(2003,500,1015,1,'2018-03-26','cart',102),(2004,500,1018,1,'2018-03-26','cart',102),(2005,500,1003,1,'2018-03-26','cart',100),(2006,500,1006,1,'2018-03-26','cart',101),(2007,500,1000,1,'2018-03-26','bought',100),(2008,500,1003,1,'2018-02-10','bought',100),(2009,500,1004,1,'2018-03-26','bought',100),(2010,500,1001,1,'2018-03-26','bought',100),(2011,500,1003,1,'2018-03-26','bought',100),(2012,500,1003,1,'2018-03-26','bought',100),(2013,500,1009,1,'2018-03-27','bought',101);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `dealerId` int(10) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `categoryId` int(10) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`productId`),
  KEY `fk_cid` (`categoryId`),
  KEY `fk_did` (`dealerId`),
  CONSTRAINT `fk_cid` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_did` FOREIGN KEY (`dealerId`) REFERENCES `dealer` (`dealerId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1019 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1000,100,'Red hand bag',1500,'Bright red handbag for women',2199,2,'../uploadFiles/100_149721124626246492-red-handbag.jpg'),(1001,100,'Green hand bag',1500,'Green handbag for women',1900,5,'../uploadFiles/100_green-bag.jpg'),(1002,100,'Blue hand bag',1500,'Bright blue handbag for women',2599,3,'../uploadFiles/100_blue-handbag.jpg'),(1003,100,'Dream catcher',1501,'Beatiful dream catcher to decorate your home',459.9,12,'../uploadFiles/100_dreamc.jpg'),(1004,100,'Aroma Diffuser',1501,'Spreads sweet odour in your room day long',1450,10,'../uploadFiles/100_diffuser1.jpg'),(1005,100,'Multicolor Carpet',1501,'Handmade carpet',4500,3,'../uploadFiles/100_mat.jpg'),(1006,101,'Color sheets',1502,'Sets of multi color sheets',349,23,'../uploadFiles/101_colorsheets1.jpg'),(1007,101,'Pencils',1502,'Set of pencils',50,5,'../uploadFiles/101_colorpencils.jpg'),(1008,101,'Erasers',1502,'Set of five erasers',38.45,4,'../uploadFiles/101_erasers1.jpg'),(1009,101,'Henko Soaps',1503,'Set of four soaps',89,20,'../uploadFiles/101_detsoap.jpg'),(1010,101,'Tide powder',1503,'Tide detergent powder extra power',120.23,10,'../uploadFiles/101_detpow.jpg'),(1011,101,'Detergent liquid',1503,'Detergent liquid for washing clothes',248.99,17,'../uploadFiles/101_detliq.jpg'),(1012,102,'Cabbage',1504,'Farm fresh cabbage',24,14,'../uploadFiles/102_cabbg.jpg'),(1013,102,'Broccoli',1504,'Fresh broccoli',78.43,5,'../uploadFiles/102_broccoli.jpg'),(1014,102,'Red bell pepper',1504,'Fresh red bell pepper grown naturally',25.67,23,'../uploadFiles/102_redbp.jpg'),(1015,102,'Green bell pepper',1504,'Fresh green bell pepper grown naturally',23.44,20,'../uploadFiles/102_gbp.jpg'),(1016,102,'Chocolate bar',1505,'Coffee flavoured chocolate bar',786.23,6,'../uploadFiles/102_chbar.jpg'),(1017,102,'Chocolate box',1505,'Contains chocolates of different flavours',678.92,3,'../uploadFiles/102_chbox.jpg'),(1018,102,'Chocolate cup',1505,'Chocolate extra loaded with almonds',68.99,12,'../uploadFiles/102_chcup.jpg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `address` varchar(200) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (500,'shekhar123','Shekhar SR  ','sshekhar@gmail.com','9848480071','Hno-3/A, Street no - 5, Ramanthapur, Hyderabad, Telangana.');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-28 13:51:12
