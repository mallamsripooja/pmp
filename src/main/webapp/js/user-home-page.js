$(document).ready(function(e) {   
    $('.addToCart').click(function(){
      var data = $(this).attr('data-target').split("-");
      $.ajax({
           type:"POST",
           url: "/user.do",
           data: { uid : data[0] , pid:data[1] , action:"addToCart"},
        });
        $(this).text("Added to cart");
        $(this).attr('class','btn btn-info');
    });

    $('.buy').click(function(){
      var data = $(this).attr('data-target').split("-");
      $.post({
           type:"POST",
           url: "/user.do",
           data: { uid : data[0] , pid:data[1] , cost:data[2], action:"buy"},
            success: function(response){
                $("html").empty();
                $("html").append(response);
            }

        });
    });
});

        