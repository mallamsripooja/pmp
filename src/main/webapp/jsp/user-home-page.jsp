<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<jsp:include page="head.jsp"></jsp:include>
<jsp:include page="user-navbar.jsp"></jsp:include>
<script type="text/javascript" src="../js/user-home-page.js"></script>
<style>
.scrollspy-example {
  position: relative;
  height: 570px;
  overflow: auto;
}
</style>
<body>
<div class="container col-lg-12">
	<div class="row">
	<div class="col-sm-2" id="spy">
	  <ul class="nav nav-pills flex-column">
	  	<c:forEach items="${categories}" var="category">
	    	<li class="nav-item"><a class="nav-link active" href="#scroll${category.categoryName}">${category.categoryName}</a></li>
	  	</c:forEach>
	  </ul>
	</div>
	<div class="col-sm-10 scrollspy-example" data-spy="scroll" data-target="#spy">
	  	<c:forEach items="${categories}" var="category">
	  		<div id="scroll${category.categoryName}">
	    		<h3><i class="fa fa-play-circle"></i> ${category.categoryName}</h3>
	    			
					<c:forEach items="${products}" var="product">
			  			<c:if test="${product.categoryId==category.categoryId}">
			  				<div class="d-inline-block">
								<div class="card" style="width: 18rem;height: 32rem;">
								  <img class="card-img-top" src="${product.imageFilePath}" alt="product image">
								  <div class="card-body">
								    <h5 class="card-title producttitle">${product.productName}</h5>
								    <p class="card-text productdescription">${product.productDescription}</p>
								    <h5 class="card-title productprice">&#x20b9; ${product.cost}</h5>
								    <button class="btn btn-success buy" type="button" role="button" data-toggle="modal" data-target="${userId}-${product.productId}-${product.cost}">Buy</button>
								    <button class="btn btn-warning addToCart" type="button" role="button" data-toggle="modal" data-target="${userId}-${product.productId}">Add to cart</button>
								    <button class="btn btn-light" role="button" data-toggle="modal" data-target="#${product.dealerId}DealerInfo"><i class="fa fa-info-circle"></i> </button>
								  </div>
								</div>
							</div>
			  			</c:if>
			  		</c:forEach>
	    		
	  		</div>
	  	</c:forEach>
	</div>
	</div>


	<c:forEach items="${dealers}" var="dealer">
		<!-- The Dealer Info Modals -->
		<div class="container">
		  <div class="modal fade" id="${dealer.id}DealerInfo">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Dealer Info</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
	        
		        <!-- Modal body -->
		        <div class="modal-body">
					<h3 class="mb-0 text-truncated">${dealer.company}</h3>
                    <p class="lead">
                    <span class="fa fa-user"></span> ${dealer.name}
                    </p>
                    <p class="lead">
                    <span class="fa fa-map-marker"></span> ${dealer.contactDetails.address}
                    </p> 
                    <p class="lead">
                    <span class="fa fa-phone"></span> ${dealer.contactDetails.contactNumber}
                    </p>
                    <p class="lead">
                    <span class="fa fa-envelope"></span> ${dealer.contactDetails.mailId}
                    </p>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>
	</c:forEach>

	</div>
</body>

</html>
