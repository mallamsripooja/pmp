<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

	<!-- Include head tag -->
	<jsp:include page="head.jsp"></jsp:include>
	
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!-- Export as PDF links -->   	
	<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
	<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>

    <!-- JS for data tables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="../js/datatable-init.js"></script>
   	
    <body>
		
		<!-- Include dealer page navbar -->
		<jsp:include page="dealer-navbar.jsp"></jsp:include>
		<br>

		<div class="container col-lg-12">
		<nav>
		  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		    <a class="nav-item nav-link active" id="nav-product-graphs-tab" data-toggle="tab" href="#nav-product-graphs" role="tab" aria-controls="nav-product-graphs" aria-selected="false">Product based Graphs</a>
		    <a class="nav-item nav-link" id="nav-graphs-tab" data-toggle="tab" href="#nav-graphs" role="tab" aria-controls="nav-graphs" aria-selected="false">Overall Graphs</a>
		    <a class="nav-item nav-link" id="nav-productStocks-tab" data-toggle="tab" href="#nav-productStocks" role="tab" aria-controls="nav-productStocks" aria-selected="true">Products Stocks</a>
		  </div>
		</nav>

		<div class="tab-content" id="nav-tabContent">
		  <br>
		  <div class="tab-pane fade show active" id="nav-product-graphs" role="tabpanel" aria-labelledby="nav-product-graphs-tab">
			  <jsp:include page="dealer-time-graph.jsp"></jsp:include>
		  </div>

		  <div class="tab-pane fade show" id="nav-graphs" role="tabpanel" aria-labelledby="nav-graphs-tab">
			  <jsp:include page="overall-dealer.jsp"></jsp:include>
		  </div>
		  <div class="tab-pane fade show" id="nav-productStocks" role="tabpanel" aria-labelledby="nav-productStock-tab">
		  			
		  	<!-- Data tables will come here -->
			<div class="container col-lg-12">
				<div class="row">
				<div class="col-md-2">
			    <button id="exportButton" class="btn btn-danger clearfix"><span class="fa fa-file-pdf-o"></span> Export to PDF</button>
				</div>
				<div class="col-lg-9">
			    <table id="exportTable" class="table table-hover table-striped table-bordered">
			        <thead>
			            <tr>
			                <th>ProductId</th>
			                <th>Name</th>
			                <th>Category</th>
			                <th>Description</th>
			                <th>Cost</th>
			                <th>Stock</th>
			                <th>Image</th>
			            </tr>
			        </thead>
			        <tbody>
						<c:forEach items="${products}" var="product">
							<tr>
								<td>${product.productId}</td>
								<td>${product.productName}</td>
								<c:forEach items="${categories}" var="category">
									<c:if test="${product.categoryId==category.categoryId}">
										<td>${category.categoryName}</td>
									</c:if>
								</c:forEach>
								<td>${product.productDescription}</td>
								<td>${product.cost}</td>
								<td>${product.stockQuantity}</td>
								<td>
							  		<img class="card-img-top" src="${product.imageFilePath}" height="80" width="80" alt="product image">
							  	</td>
							</tr>
						</c:forEach>
			        </tbody>

			    </table>
			    </div>
			    </div>
			</div>
			
			<!-- you need to include the shieldui css and js assets in order for the components to work -->
			<link rel="stylesheet" type="text/css" href="https://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
			<script type="text/javascript" src="https://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
			<script type="text/javascript" src="https://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>
			
			<script type="text/javascript">
			    jQuery(function ($) {
			        $("#exportButton").click(function () {
			            // parse the HTML table element having an id=exportTable
			            var dataSource = shield.DataSource.create({
			                data: "#exportTable",
			                schema: {
			                    type: "table",
			                    fields: {
			                    	ProductId: { type: Number },
			                    	Name: { type: String },
			                    	Category: { type: String },
			                    	Description: { type: String },
			                    	Cost: { type: Number },
			                    	Stock: { type: Number }
			                    }
			                }
			            });
			
			            // when parsing is done, export the data to PDF
			            dataSource.read().then(function (data) {
			                var pdf = new shield.exp.PDFDocument({
			                    author: "PrepBootstrap",
			                    created: new Date()
			                });
			
			                pdf.addPage("a4", "portrait");
			
			                pdf.table(
			                    50,
			                    50,
			                    data,
			                    [
			                        { field: "ProductId", title: "ProductId", width: 70 },
			                        { field: "Name", title: "Name", width: 70 },
			                        { field: "Category", title: "Category", width: 70 },
			                        { field: "Description", title: "Description", width: 200 },
			                        { field: "Cost", title: "Cost", width: 70 },
			                        { field: "Stock", title: "Stock", width: 50 }
			                    ],
			                    {
			                        margins: {
			                            top: 50,
			                            left: 50
			                        }
			                    }
			                );
			
			                pdf.saveAs({
			                    fileName: "ProductsStockPDF"
			                });
			            });
			        });
			    });
			</script>
		  				
		  </div>
		  	  
		</div>
	</div>

	<!-- The add catgeory Modal -->
		<div class="container">
		  <div class="modal fade" id="addCategoryModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Add new category</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="dealer.do" method="post">
		        <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
						<div class="row">
					 		<div class="form-group">
					 			<div class="col-md">
							   		<label for="categoryName">Category name</label>
							   		<input autofocus type="text" class="form-control" id="categoryName" name="categoryName" pattern="[ A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed" required>
					 			</div>
					 		</div>
						</div>
					  	<div class="row">
					 		<div class="col-md">
					 		<button type="submit" class="btn btn-primary" name="addCategorySubmit">Submit</button>
					 		</div>
						</div>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- The add product Modal -->
		<div class="container">
		  <div class="modal fade" id="addProductModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Add new product</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="dealer.do" method="post" enctype="multipart/form-data">
		        <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
					<div class="form-group">  
					  <div class="col-md">
					  	<label for="productName">Product Name</label>
						<input autofocus id="productName" name="productName" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,30}" title="Three to thirty chars long, only alphabets allowed">				    
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productDescription">Product Description</label>  
					    <input id="productDescription" name="productDescription" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,50}" title="Three to fifty chars long, only alphabets allowed">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productCost">Product Cost</label>  
					    <input id="productCost" name="productCost" class="form-control input-md" required type="number" step="any" min="10">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productCategory">Product Category</label>
						<input id="productCategory" name="productCategory" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productQuantity">Available Quantity</label>  
					    <input id="productQuantity" name="productQuantity" class="form-control input-md" required type="text" type="number" min="1">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
			            <div class="input-group image-preview">
			                <input type="text" class="form-control image-preview-filename" disabled="disabled">
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
			                        <span class="glyphicon glyphicon-remove"></span> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default image-preview-input">
			                        <span class="glyphicon glyphicon-folder-open"></span>
			                        <span class="image-preview-input-title">Browse</span>
			                        <input type="file" accept="image/png, image/jpeg, image/gif" name="productImage"/>
			                    </div>
			                </span>
			            </div>
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
				 		<button type="submit" class="btn btn-primary" name="addProductSubmit">Submit</button>
					  </div>
					</div>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>

	</body>
</html>