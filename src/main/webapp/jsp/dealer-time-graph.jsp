<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form method="post" action="/dealer.do">
      <div class="container">
        <div class="col-lg-12">
            <div class="form-group">
              <input type='number' name="pid" min=1000 required class="form-control" placeholder="Enter the product Id" value="${pid}" />
            </div>
            <input type="submit" class="btn btn-success" name="timeBasedDealerGraph"/>
        </div>
      </div>
    </form>
     <c:if test="${not empty productList}">
            <div id="div"></div>
   
            <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
    
  google.charts.setOnLoadCallback(drawGraph);

function drawGraph() {
   var data = google.visualization.arrayToDataTable([
                  ['Date', 'Units sold'],
                  <c:forEach items="${productList}" var="data">
                      [ '${data.getDate()}', ${data.getQuantity()} ],
                  </c:forEach>
            ]);

            var options = {
        title: 'Sales report for selected product',
        height:500,
        weight:500,
        curveType:'function',
        legend:{position:'bottom'}
      };

      var chart = new google.visualization.LineChart(document.getElementById('div'));

      chart.draw(data, options);
  }
      </script>

    </c:if>

    <c:if test="${empty pid}">
       <div class="alert alert-danger">Enter productId to view graph</div>
    </c:if>  

    <c:if test="${IFNULL != pid && empty productList}">
       <div class="alert alert-danger">No data</div>
    </c:if>  
