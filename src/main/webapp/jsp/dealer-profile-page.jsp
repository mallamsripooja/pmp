<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<!-- Include head tag -->
<jsp:include page="head.jsp"></jsp:include>

<body>

	<!-- Include dealer page navbar -->
	<jsp:include page="dealer-navbar.jsp"></jsp:include>
	<br>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <h3 class="mb-0 text-truncated">${dealerDetails.company}</h3>
                            <p class="lead">
                            <span class="fa fa-user"></span> ${dealerDetails.name} | 
                            <span class="fa fa-map-marker"></span> ${dealerDetails.contactDetails.address} | 
                            <span class="fa fa-phone"></span> ${dealerDetails.contactDetails.contactNumber} | 
                            <span class="fa fa-envelope"></span> ${dealerDetails.contactDetails.mailId}
                            </p>
                            <p>
                                Categories added &nbsp;
                                <c:forEach items="${categories}" var="category">
                                	<span class="badge badge-danger tags">${category.categoryName}</span>
                                </c:forEach>
                            </p>
                        </div>
                        </div>
                        <br><br><br>
                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <h3 class="mb-0">${fn:length(products)}</h3>
                            <small>products addded</small>
                        </div>
                        <div class="col-12 col-lg-4">
                            <h3 class="mb-0">${fn:length(categories)}</h3>
                            <small>categories added</small>
                        </div>
                        <div class="col-12 col-lg-3">
                            <button type="button" class="btn btn-outline-primary btn-block"data-toggle="modal"
							data-target="#${userId}Edit"><span class="fa fa-pencil"></span> Edit profile</button>
                        </div>
                        <!--/col-->
                    </div>
                    <!--/row-->
                </div>
                <!--/card-block-->
            </div>
        </div>
    </div>

	<div class="modal fade" id="${userId}Edit">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Edit profile</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<form method="post" action="/dealer.do">
					<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
						<div class="col-sm-12">
							<div class="form-group">
								<label>Name</label> <input autofocus type="text"
									name="firstName" class="form-control"
									value="${dealerDetails.name}" pattern="[ A-Za-z]{3,30}" title="Three to thirty chars long, only alphabets allowed" required> <input
									type="hidden" name="lastName" value="">
									<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea rows="2" name="address" class="form-control" required>${dealerDetails.contactDetails.address}</textarea>
							</div>
							<div class="form-group">
								<label>Phone Number</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">+91</span>
									</div>
									<input type="text" class="form-control" name="mobile"
										aria-label="phonenumber" aria-describedby="basic-addon1"
										value="${dealerDetails.contactDetails.contactNumber}" pattern="[6789][0-9]{9}" title="10 digit mobile number. Can start with 6,7,8,9" required>
								</div>
							</div>
							<div class="form-group">
								<label>Email Address</label> <input type="email"
									value="${dealerDetails.contactDetails.mailId}" name="email"
									class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
							</div>
							<button type="submit" name="updateDealerDetails"
								class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
<!-- The add catgeory Modal -->
		<div class="container">
		  <div class="modal fade" id="addCategoryModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Add new category</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="dealer.do" method="post">
		        <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
						<div class="row">
					 		<div class="form-group">
					 			<div class="col-md">
							   		<label for="categoryName">Category name</label>
							   		<input autofocus type="text" class="form-control" id="categoryName" name="categoryName" pattern="[ A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed" required>
					 			</div>
					 		</div>
						</div>
					  	<div class="row">
					 		<div class="col-md">
					 		<button type="submit" class="btn btn-primary" name="addCategorySubmit">Submit</button>
					 		</div>
						</div>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- The add product Modal -->
		<div class="container">
		  <div class="modal fade" id="addProductModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Add new product</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="dealer.do" method="post" enctype="multipart/form-data">
					<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
					<div class="form-group">  
					  <div class="col-md">
					  	<label for="productName">Product Name</label>
						<input autofocus id="productName" name="productName" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,30}" title="Three to thirty chars long, only alphabets allowed">				    
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productDescription">Product Description</label>  
					    <input id="productDescription" name="productDescription" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,50}" title="Three to fifty chars long, only alphabets allowed">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productCost">Product Cost</label>  
					    <input id="productCost" name="productCost" class="form-control input-md" required type="number" step="any" min="10">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productCategory">Product Category</label>
						<input id="productCategory" name="productCategory" class="form-control input-md" required type="text" pattern="[ A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
					  	<label for="productQuantity">Available Quantity</label>  
					    <input id="productQuantity" name="productQuantity" class="form-control input-md" required type="text" type="number" min="1">
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
			            <div class="input-group image-preview">
			                <input type="text" class="form-control image-preview-filename" disabled="disabled">
			                <span class="input-group-btn">
			                    <!-- image-preview-clear button -->
			                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
			                        <span class="glyphicon glyphicon-remove"></span> Clear
			                    </button>
			                    <!-- image-preview-input -->
			                    <div class="btn btn-default image-preview-input">
			                        <span class="glyphicon glyphicon-folder-open"></span>
			                        <span class="image-preview-input-title">Browse</span>
			                        <input type="file" accept="image/png, image/jpeg, image/gif" name="productImage"/>
			                    </div>
			                </span>
			            </div>
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md">
				 		<button type="submit" class="btn btn-primary" name="addProductSubmit">Submit</button>
					  </div>
					</div>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>

</body>
</html>