<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
			<c:if test="${not empty salesList}">
        		<div id="chart_div"></div>
	
      			<script type="text/javascript">
      				google.charts.load('current', {packages: ['corechart']});
      	google.charts.setOnLoadCallback(drawBasic);

      function drawBasic() {
      	 var data = google.visualization.arrayToDataTable([
                        ['Products', 'Units sold'],
                        <c:forEach items="${salesList}" var="data">
                            [ '${data.getProductName()}', ${data.getStockQuantity()} ],
                        </c:forEach>

                  ]);

                  var options = {
              
              title: 'Overall sales report',
              height: 800,
              width: 1000,
              is3D:true
            };

            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

            	chart.draw(data, options);
        }
      			</script>
			</c:if>

			<c:if test="${empty salesList}">
			   <div class="alert alert-danger">No data</div>
			</c:if>
