<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<!-- Include head tag -->
<jsp:include page="head.jsp"></jsp:include>

<body>

	<!-- Include dealer page navbar -->
	<jsp:include page="user-navbar.jsp"></jsp:include>
	<br>

	<div class="container" id="accordion">
	  
	  <c:forEach items="${dealers}" var="dealer">
	  <div class="card">
	  	<!-- Head part -->
	    <div class="card-header" id="heading${dealer.id}">
	      <h5 class="mb-0">
	        <button class="btn col-lg-12" data-toggle="collapse" data-target="#collapse${dealer.id}" aria-expanded="false" aria-controls="collapse${dealer.id}">
	        	<div>
					<h3 class="mb-0 text-truncated">${dealer.company}</h3>
                    <p class="lead">
                    <span class="fa fa-user"></span> ${dealer.name}
                    </p>
                    <p class="lead">
                    <span class="fa fa-map-marker"></span> ${dealer.contactDetails.address}
                    </p> 
                    <p class="lead">
                    <span class="fa fa-phone"></span> ${dealer.contactDetails.contactNumber}
                    </p>
                    <p class="lead">
                    <span class="fa fa-envelope"></span> ${dealer.contactDetails.mailId}
                    </p>
		        </div>
	        </button>
	      </h5>
	    </div>
	    
	    <!-- Collapsible part -->
	    <div id="collapse${dealer.id}" class="collapse" aria-labelledby="heading${dealer.id}" data-parent="#accordion">
	      <div class="card-body">
     		<c:forEach items="${products}" var="product">
	  			<c:if test="${product.dealerId==dealer.id}">
	  				<div class="d-inline-block">
						<div class="card" style="width: 18rem;height: 28rem;">
						  <img class="card-img-top" src="${product.imageFilePath}" alt="product image">
						  <div class="card-body">
						    <h5 class="card-title producttitle">${product.productName}</h5>
						    <p class="card-text productdescription">${product.productDescription}</p>
						    <h5 class="card-title productprice">&#x20b9; ${product.cost}</h5>
						  </div>
						</div>
					</div>
	  			</c:if>
	  		</c:forEach>
	      </div>
	    </div>
	  </div>
	  </c:forEach>
	</div>

</body>
</html>