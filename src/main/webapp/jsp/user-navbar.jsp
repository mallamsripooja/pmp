<nav class="navbar sticky-top navbar-light bg-light">
  <a class="navbar-brand" href="user.do">
    <img src="../images/cicon.png" width="30" height="30" class="d-inline-block align-top" alt="">
    e-Cart
  </a>
  <ul class="nav justify-content-end">
	  <li class="nav-item">
	  <form action="user.do" method="post">
	  <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
		<div class="input-group mb-3">
		  <input type="text" class="form-control" name="searchInput" placeholder="Search for product" aria-label="Search for product" aria-describedby="basic-addon2" pattern="[ A-Za-z]{3,20}" title="Three to twenty chars long, only alphabets allowed" autocomplete="off">
		  <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
		  <div class="input-group-append">
		    <button class="btn btn-dark" type="submit" name="searchProductsSubmit"><i class="fa fa-search"></i></button>
		  </div>
		</div>
	  </form>
	  </li>
	  &emsp;
	  <li class="nav-item">
	  <form action="user.do" method="post">
	  	<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
	  	<button type="submit" class="btn btn-info" name="viewDealersSubmit">
		  <i class="fa fa-users"></i> View Dealers
		</button>
	  </form>
	  </li>
	  &emsp;
	  <li class="nav-item">
	  <form action="user.do" method="post">
	  <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
	  	<button type="submit" class="btn btn-info" name="viewCartSubmit">
		  <i class="fa fa-shopping-cart"></i> View cart
		</button>
	  </form>
	  </li>
	  &emsp;
	  <li class="nav-item">
	  <form action="user.do" method="post">
	  <input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
	  	<button type="submit" class="btn btn-info" name="viewProfileSubmit">
		  <i class="fa fa-user"></i> View profile
		</button>
	  </form>
	  </li>
	  &emsp;
	  <li class="nav-item">
	  	<a href="/logout" class="btn btn-primary"><i class="fa fa-sign-out"></i> Logout</a>
	  </li>
  </ul>
</nav>	