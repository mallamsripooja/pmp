<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<!-- Include head tag -->
<jsp:include page="head.jsp"></jsp:include>

<body>

	<!-- Include dealer page navbar -->
	<jsp:include page="user-navbar.jsp"></jsp:include>
	<br>
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <p class="lead">
                            <span class="fa fa-user"></span>&emsp; ${userDetails.name}
                            </p>
                            <p class="lead">
                            <span class="fa fa-map-marker"></span>&emsp; ${userDetails.contactDetails.address}
                            </p>
                            <p class="lead">
                            <span class="fa fa-phone"></span>&emsp; ${userDetails.contactDetails.contactNumber}
                            </p>
                            <p class="lead">
                            <span class="fa fa-envelope"></span>&emsp; ${userDetails.contactDetails.mailId}
                            </p>
                        </div>
                        </div>
                        <br>
                    <div class="row">
                        <div class="col-12 col-lg-3">
                            <button type="button" class="btn btn-outline-primary btn-block"data-toggle="modal"
							data-target="#${userId}Edit"><span class="fa fa-pencil"></span> Edit profile</button>
                        </div>
                        <!--/col-->
                    </div>
                    <!--/row-->
                </div>
                <!--/card-block-->
            </div>
        </div>
    </div>

	<div class="modal fade" id="${userId}Edit">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Edit profile</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<form method="post" action="/user.do">
					<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
						<div class="col-sm-12">
							<div class="form-group">
								<label>Name</label> <input autofocus type="text"
									name="firstName" class="form-control"
									value="${userDetails.name}" pattern="[ A-Za-z]{3,30}" title="Three to thirty chars long, only alphabets allowed" required> <input
									type="hidden" name="lastName" value="">
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea rows="2" name="address" class="form-control" required>${userDetails.contactDetails.address}</textarea>
							</div>
							<div class="form-group">
								<label>Phone Number</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">+91</span>
									</div>
									<input type="text" class="form-control" name="mobile"
										aria-label="phonenumber" aria-describedby="basic-addon1"
										value="${userDetails.contactDetails.contactNumber}" pattern="[6789][0-9]{9}" title="10 digit mobile number. Can start with 6,7,8,9" required>
								</div>
							</div>
							<div class="form-group">
								<label>Email Address</label> <input type="email"
									value="${userDetails.contactDetails.mailId}" name="email"
									class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
							</div>
							<button type="submit" name="updateUserDetails"
								class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>