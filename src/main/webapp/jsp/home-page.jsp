<html>

	<!-- Include head tag -->
	<jsp:include page="head.jsp"></jsp:include>

    <body>
		<nav class="navbar fixed-top navbar-light bg-light">
		  <a class="navbar-brand" href="login.do">
		    <img src="../images/cicon.png" width="30" height="30" class="d-inline-block align-top" alt="">
		    e-Cart
		  </a>
		  <ul class="nav justify-content-end">
			  <li class="nav-item">
			  	${error}
			  </li>
			  <li class="nav-item">
			  &emsp;
			  &emsp;
			  &emsp;
			  <li class="nav-item">
			  	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#userLoginModal">
				  Buy products
				</button>
			  </li>
			  <li class="nav-item">
			  &emsp;
			  </li>
			  <li class="nav-item">
			  	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dealerLoginModal">
				  Sell products
				</button>
			  </li>
		  </ul>
		</nav>   
		
		<!-- The User Login Modal -->
		<div class="container">
		  <div class="modal fade" id="userLoginModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Login</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="login.do" method="post">
		        	<div class="row">
					<div class="form-group">
						<div class="col-md">
							<label for="userId">User Id</label>
							<input autofocus type="text" class="form-control" id="userId" name="userId" placeholder="Enter user Id" pattern="[0-9]{3}" title="Three digit id" required>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="form-group">
					<div class="col-md">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" title="Minimum eight characters, at least one letter and one number" required>
					</div>
					</div>
					</div>
		        	<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
				  	<div class="row">
				 		<div class="col-md">
					 		<button type="submit" class="btn btn-primary" name="userLoginSubmit">Submit</button>
					 	</div>
					</div>
		        </form>
		        <form action="register" method="post">
		        	<button type="submit" class="btn btn-link" name="registerUserPage">Not yet registered?</button>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- The Dealer Login Modal -->
		<div class="container">
		  <div class="modal fade" id="dealerLoginModal">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Login</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        <form action="login.do" method="post">
		        	<div class="row">
					<div class="form-group">
						<div class="col-md">
							<label for="userId">User Id</label>
							<input autofocus type="text" class="form-control" id="userId" name="userId" placeholder="Enter user Id" pattern="[0-9]{3}" title="Three digit id" required>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="form-group">
					<div class="col-md">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern="[0-9]{5}" title="Five digit password" required>
					</div>
					</div>
					</div>
		        	<input id="token" class="form-control" name="token" type="hidden" value="${sessionScope.csrfToken}" />
				  	<div class="row">
				 		<div class="col-md">
					 		<button type="submit" class="btn btn-primary" name="dealerLoginSubmit">Submit</button>
					 	</div>
					</div>
		        </form>
		        <form action="register" method="post">
		        	<button class="btn btn-link" name="registerDealerPage">Not yet registered?</button>
		        </form>
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>
		
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  </ol>
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img class="d-block w-100" src="../images/shop1.jpg" alt="First slide">
		      <div class="carousel-caption d-none d-md-block">
			    <h1><strong><i class="fa fa-money"></i> Buy products</strong></h1>
			    <h3><strong><em>We offer wide variety of products</em></strong></h3>
			  </div>
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="../images/shop2.jpg" alt="Second slide">
		      <div class="carousel-caption d-none d-md-block">
			    <h1><strong><i class="fa fa-shopping-bag"></i> Sell products</strong></h1>
			    <h3><strong><em>You run a small scale business and worried about selling your products online? We are there to help you!</em></strong></h3>
			  </div>
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="../images/shop3.jpg" alt="Third slide">
		    </div>
		  </div>
		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
		
    </body>
</html>