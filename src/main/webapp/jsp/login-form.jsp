<div class="row">
<div class="form-group">
	<div class="col-md">
		<label for="userId">UserId</label>
		<input autofocus type="text" class="form-control" id="userId" name="userId" placeholder="Enter user Id" pattern="[0-9]{3}" title="Three digit id" required>
	</div>
</div>
</div>
<div class="row">
<div class="form-group">
<div class="col-md">
		<label for="password">Password</label>
		<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
</div>
</div>
</div>