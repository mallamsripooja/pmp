<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<jsp:include page="head.jsp"></jsp:include>
<body>

    <jsp:include page="navbar-without-buttons.jsp"></jsp:include>
	<br>
	<c:choose>
		<c:when test="{message=='Error'}">
	    	<div class="alert alert-danger">
	    		Could not register!
	    	</div>
	    </c:when>

		<c:when test="${not empty message}">
			<div class="alert alert-success">
	    		Registered successfully with <strong>User Id ${message}</strong>.<br>
	    		This the <strong>User Id</strong> you must use to login to our website.<br>
	    		Thanks for choosing our portal!
	    	</div>
	    </c:when> 
	    
	    <c:otherwise>
			<div class="container col-md-7">
				<div class="col-md-10">
					<div class="row">
						<form method="post" id="userForm" class="margin10" action="/register">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-6 form-group">
										<label>First Name</label>
										<input name="firstName" type="text" pattern="[A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed" class="form-control" required>
									</div>
									<div class="col-sm-6 form-group">
										<label>Last Name</label>
										<input name="lastName" type="text" pattern="[A-Za-z]{3,10}" title="Three to ten chars long, only alphabets allowed" class="form-control" required>
									</div>
								</div>					
								<div class="form-group">
									<label>Address</label>
									<textarea  name="address" required rows="2" class="form-control"></textarea>
								</div>	
								<div class="form-group">
									<label>Phone Number</label>
									<div class="input-group mb-3">
									  <div class="input-group-prepend">
									    <span class="input-group-text" id="basic-addon1">+91</span>
									  </div>
									  <input type="text" class="form-control" name="mobile" aria-label="phonenumber" aria-describedby="basic-addon1" pattern="[6789][0-9]{9}" title="10 digit mobile number. Can start with 6,7,8,9" required>
									</div>
								</div>		
								<div class="form-group">
									<label>Email Address</label>
									<input name="email" type="email" required placeholder="sample@company.com" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
								</div>	
								<div class="form-group">
									<label>Enter password</label>
									<input name="password" id="password" type="password" required class="form-control" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" title="Minimum eight characters, at least one letter and one number">
								</div>
								<div id="responseDiv"></div>
								<div class="form-group">
									<input type="checkbox" aria-label="Checkbox for following text input" required>
									<a href="#" data-toggle="modal" data-target="#userPrivacyPolicyModal">
									&nbsp;I read privacy policies of this company.
									</a>
								</div>
								<input type="hidden" name="action" id="action"/>
								<button type="submit" name="registerUser" id="submitButton" class="btn btn-info">
									Submit
								</button>					
							</div>
						</form> 
					</div>
				</div>
			</div>
	    </c:otherwise>
	</c:choose>

		<!-- The terms and condtions Modal -->
		<div class="container">
		  <div class="modal fade" id="userPrivacyPolicyModal">
		    <div class="modal-dialog modal-lg">
		      <div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		          <h4 class="modal-title">Terms & Conditions</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body">
		        	Terms and conditions will be defined here.		        
		        </div>
		        
		      </div>
		    </div>
		  </div>
		</div>

</body>
</html>