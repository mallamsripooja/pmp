package com.pmp.sql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

public class DatabaseConnector {

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbConnectionInstance;

	public Connection getDBConnection() {
		System.out.println("inside getconnectiondb");
		try {
			dbConnectionInstance = ds.getConnection();
			System.out.println(dbConnectionInstance);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dbConnectionInstance;
	}

}
