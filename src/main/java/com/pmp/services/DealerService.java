package com.pmp.services;

import static com.pmp.sql.SQLQueries.GET_ANNUAL_DEALER_SALES_DATA;
import static com.pmp.sql.SQLQueries.GET_DATEWISE_DEALER_SALES_DATA;
import static com.pmp.sql.SQLQueries.GET_DEALER_SALES_DATA;
import static com.pmp.sql.SQLQueries.GET_REQUEST_ID;
import static com.pmp.sql.SQLQueries.INSERT_INTO_CATEGORY_TABLE;
import static com.pmp.sql.SQLQueries.INSERT_INTO_DEALER_DETAILS;
import static com.pmp.sql.SQLQueries.INSERT_INTO_PRODUCT_TABLE;
import static com.pmp.sql.SQLQueries.SELECT_ALL_DEALERS;
import static com.pmp.sql.SQLQueries.SELECT_DEALER_BY_DEALERID;
import static com.pmp.sql.SQLQueries.SELECT_FROM_DEALER_TABLE;
import static com.pmp.sql.SQLQueries.UPDATE_DEALER_BY_DEALERID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pmp.elements.ContactDetails;
import com.pmp.elements.Dealer;
import com.pmp.elements.Order;
import com.pmp.elements.Product;

public class DealerService {
	private Connection dbconnection;

	public DealerService(Connection dbconnection) {
		this.dbconnection = dbconnection;
	}

	public boolean isDealerValid(int userId, String password) {
		try {
			ResultSet rs = dbconnection.prepareStatement(SELECT_FROM_DEALER_TABLE).executeQuery();
			while (rs.next()) {
				if (rs.getInt("dealerId") == userId && rs.getString("password").equals(password)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void addNewCategory(String categoryName, int dealerId) {
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(INSERT_INTO_CATEGORY_TABLE);
			preparedStmt.setString(1, categoryName);
			preparedStmt.setInt(2, dealerId);
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addNewProduct(Product product) {
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(INSERT_INTO_PRODUCT_TABLE);
			preparedStmt.setInt(1, product.getDealerId());
			preparedStmt.setString(2, product.getProductName());
			preparedStmt.setInt(3, product.getCategoryId());
			preparedStmt.setString(4, product.getProductDescription());
			preparedStmt.setFloat(5, product.getCost());
			preparedStmt.setInt(6, product.getStockQuantity());
			preparedStmt.setString(7, product.getImageFilePath());

			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int insertDealerDetails(Dealer dealer) {
		try {
			PreparedStatement stmt = dbconnection.prepareStatement(INSERT_INTO_DEALER_DETAILS);
			stmt.setInt(1, -1);
			stmt.setString(2, dealer.getName());
			stmt.setString(3, dealer.getContactDetails().getAddress());
			stmt.setString(4, dealer.getContactDetails().getMailId());
			stmt.setString(5, dealer.getContactDetails().getContactNumber());
			stmt.setString(6, dealer.getCompany());
			stmt.executeUpdate();
			ResultSet rs = dbconnection.prepareStatement(GET_REQUEST_ID).executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public Dealer getDealerByDealerId(int dealerId) {
		Dealer dealer = new Dealer();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(SELECT_DEALER_BY_DEALERID);
			preparedStmt.setInt(1, dealerId);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				dealer.setName(rs.getString("name"));
				dealer.setCompany(rs.getString("company"));
				ContactDetails contactDetails = new ContactDetails();
				contactDetails.setAddress(rs.getString("area"));
				contactDetails.setContactNumber(rs.getString("mobile"));
				contactDetails.setMailId(rs.getString("email"));
				dealer.setContactDetails(contactDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dealer;
	}

	public void updateDealerDetails(int dealerId, Dealer dealer) {
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(UPDATE_DEALER_BY_DEALERID);
			preparedStmt.setString(1, dealer.getName());
			preparedStmt.setString(2, dealer.getContactDetails().getAddress());
			preparedStmt.setString(3, dealer.getContactDetails().getMailId());
			preparedStmt.setString(4, dealer.getContactDetails().getContactNumber());
			preparedStmt.setInt(5, dealerId);
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Dealer> getAllDealers() {
		List<Dealer> dealers = new ArrayList<Dealer>();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(SELECT_ALL_DEALERS);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs != null && rs.next()) {
				Dealer dealer = new Dealer();
				dealer.setId(rs.getInt("dealerId"));
				dealer.setName(rs.getString("name"));
				dealer.setCompany(rs.getString("company"));
				ContactDetails contactDetails = new ContactDetails();
				contactDetails.setAddress(rs.getString("area"));
				contactDetails.setContactNumber(rs.getString("mobile"));
				contactDetails.setMailId(rs.getString("email"));
				dealer.setContactDetails(contactDetails);
				dealers.add(dealer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dealers;
	}

	public List<Product> getSalesData(int dealerId) {
		List<Product> salesList = new ArrayList<Product>();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(GET_DEALER_SALES_DATA);
			preparedStmt.setInt(1, dealerId);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setProductName(rs.getString(1));
				product.setStockQuantity(rs.getInt(2));
				salesList.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salesList;
	}

	public List<Product> getDateWiseSalesData(int dealerId, String date) {
		List<Product> salesList = new ArrayList<Product>();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(GET_DATEWISE_DEALER_SALES_DATA);
			preparedStmt.setInt(1, dealerId);
			preparedStmt.setString(2, date);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setProductName(rs.getString(1));
				product.setStockQuantity(rs.getInt(2));
				salesList.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salesList;
	}

	public List<Order> getAnnualSalesData(int dealerId, int productId) {

		List<Order> salesList = new ArrayList<Order>();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(GET_ANNUAL_DEALER_SALES_DATA);
			preparedStmt.setInt(1, dealerId);
			preparedStmt.setInt(2, productId);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setDate(getMonth(rs.getString(1)));
				order.setQuantity(rs.getInt(2));
				salesList.add(order);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salesList;
	}

	private String getMonth(String date) {
		String month = date.split("-")[0];
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("01", "JAN");
		hm.put("03", "MAR");
		hm.put("05", "MAY");
		hm.put("07", "JULY");
		hm.put("09", "SEP");
		hm.put("11", "NOV");
		hm.put("02", "FEB");
		hm.put("04", "APR");
		hm.put("06", "JUN");
		hm.put("08", "AUG");
		hm.put("10", "OCT");
		hm.put("12", "DEC");
		return hm.get(month);
	}

}
