package com.pmp.services;

import static com.pmp.sql.SQLQueries.DELETE_TRANSACTION;
import static com.pmp.sql.SQLQueries.GET_DEALER_FROM_PRODUCT;
import static com.pmp.sql.SQLQueries.GET_PRODUCT_DETAIL;
import static com.pmp.sql.SQLQueries.GET_USER_ID;
import static com.pmp.sql.SQLQueries.GET_USER_ORDERS;
import static com.pmp.sql.SQLQueries.INSERT_INTO_ORDER;
import static com.pmp.sql.SQLQueries.INSERT_INTO_USER_DETAILS;
import static com.pmp.sql.SQLQueries.SELECT_FROM_USER_TABLE;
import static com.pmp.sql.SQLQueries.SELECT_USER_BY_USERID;
import static com.pmp.sql.SQLQueries.UPDATE_TRANSACTION;
import static com.pmp.sql.SQLQueries.UPDATE_USER_BY_USERID;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pmp.elements.ContactDetails;
import com.pmp.elements.Order;
import com.pmp.elements.Product;
import com.pmp.elements.User;

public class UserService {
	private Connection dbconnection;

	public UserService(Connection dbconnection) {
		this.dbconnection = dbconnection;
	}

	public boolean isUserValid(int userId, String password) {
		try {
			ResultSet rs = dbconnection.prepareStatement(SELECT_FROM_USER_TABLE).executeQuery();
			while (rs.next()) {
				if (rs.getInt("userId") == userId && rs.getString("password").equals(password)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public int insertUserDetails(String firstName, String lastName, String address, String mobile, String email,
			String password, Connection dbconnection) {
		try {
			PreparedStatement stmt = dbconnection.prepareStatement(INSERT_INTO_USER_DETAILS);
			stmt.setString(1, password);
			stmt.setString(2, firstName + " " + lastName);
			stmt.setString(3, email);
			stmt.setString(4, mobile);
			stmt.setString(5, address);
			stmt.executeUpdate();
			ResultSet rs = dbconnection.prepareStatement(GET_USER_ID).executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public User getUserByUserId(int userId) {
		User user = new User();
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(SELECT_USER_BY_USERID);
			preparedStmt.setInt(1, userId);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				user.setName(rs.getString("name"));
				ContactDetails contactDetails = new ContactDetails();
				contactDetails.setAddress(rs.getString("address"));
				contactDetails.setContactNumber(rs.getString("mobile"));
				contactDetails.setMailId(rs.getString("email"));
				user.setContactDetails(contactDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public void updateUserDetails(int userId, User user) {
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(UPDATE_USER_BY_USERID);
			preparedStmt.setString(1, user.getName());
			preparedStmt.setString(2, user.getContactDetails().getAddress());
			preparedStmt.setString(3, user.getContactDetails().getMailId());
			preparedStmt.setString(4, user.getContactDetails().getContactNumber());
			preparedStmt.setInt(5, userId);
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertIntoCart(int userId, int productId, String type) {
		try {
			PreparedStatement preparedStmt = dbconnection.prepareStatement(GET_DEALER_FROM_PRODUCT);
			preparedStmt.setInt(1, productId);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				int dealerId = rs.getInt(1);
				preparedStmt = dbconnection.prepareStatement(INSERT_INTO_ORDER);
				preparedStmt.setInt(1, userId);
				preparedStmt.setInt(2, productId);
				preparedStmt.setInt(3, 1);
				preparedStmt.setDate(4, new Date((new java.util.Date().getTime())));
				preparedStmt.setString(5, type);
				preparedStmt.setInt(6, dealerId);
				preparedStmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Order> getAllOrders(int userId) {
		List<Order> orderList = new ArrayList<Order>();
		try {
			PreparedStatement stmt = dbconnection.prepareStatement(GET_USER_ORDERS);
			stmt.setInt(1, userId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setProductId(rs.getInt(3));
				PreparedStatement pstmt = dbconnection.prepareStatement(GET_PRODUCT_DETAIL);
				pstmt.setInt(1, product.getProductId());
				ResultSet ps = pstmt.executeQuery();
				if (ps.next()) {
					product.setProductName(ps.getString(1));
					product.setProductDescription(ps.getString(2));
					product.setCost(ps.getFloat(3));
					product.setImageFilePath(ps.getString(4));
				}
				Order order = new Order();
				order.setTransactionId(rs.getInt(1));
				order.setUserId(rs.getInt(2));
				order.setProduct(product);
				order.setQuantity(rs.getInt(4));
				order.setTotal(order.getQuantity() * order.getProduct().getCost());

				orderList.add(order);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orderList;
	}

	public void deleteTransaction(int transactionId) {
		try {
			PreparedStatement stmt = dbconnection.prepareStatement(DELETE_TRANSACTION);
			stmt.setInt(1, transactionId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateTransaction(int transactionId, int quantity) {
		try {
			PreparedStatement stmt = dbconnection.prepareStatement(UPDATE_TRANSACTION);
			stmt.setInt(1, quantity);
			stmt.setInt(2, transactionId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
