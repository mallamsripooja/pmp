package com.pmp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.pmp.elements.Dealer;
import com.pmp.elements.Order;
import com.pmp.services.AdminService;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbconnection;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("adminId") == null)
			request.getRequestDispatcher("jsp/admin-login.jsp").forward(request, response);
		else
			homePage(request, response);

	}

	protected void homePage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		AdminService adminService = null;

		try {
			dbconnection = ds.getConnection();
			adminService = new AdminService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		List<Dealer> requestList = adminService.dealerRequestList();
		request.setAttribute("requestList", requestList);
		request.getRequestDispatcher("jsp/admin-home-page.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		AdminService adminService = null;

		try {
			dbconnection = ds.getConnection();
			adminService = new AdminService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String action = request.getParameter("action");
		boolean result;

		HttpSession session = request.getSession(false);

		if (action.equals("adminLogin")) {
			String id = request.getParameter("admin");
			String pwd = request.getParameter("password");
			if (adminService.login(id, pwd)) {
				request.getSession().setAttribute("adminId", id);
				response.setHeader("Set-Cookie", "ADMINID=" + id + ";");
				homePage(request, response);
			} else {
				request.setAttribute("error_message", "invalid");
				doGet(request, response);
			}
		} else if (session == null || session.getAttribute("adminId") == null) {
			request.getRequestDispatcher("jsp/admin-login.jsp").forward(request, response);
		} else if (action.equals("accept")) {
			int requestId = Integer.parseInt(request.getParameter("rid"));
			result = adminService.acceptRequest(requestId);
		} else if (action.equals("reject")) {
			int requestId = Integer.parseInt(request.getParameter("rid"));
			result = adminService.rejectRequest(requestId);
		} else if (action.equals("getDealer")) {
			int dealerId = Integer.parseInt(request.getParameter("did"));
			Dealer dealer = adminService.getDealerRecord(dealerId);
			if (dealer == null)
				request.setAttribute("msg", "null");
			else
				request.setAttribute("dealer", dealer);
			request.getRequestDispatcher("jsp/unregister-dealer.jsp").forward(request, response);
		} else if (action.equals("inbox")) {
			homePage(request, response);
		} else if (action.equals("unregister")) {
			request.getRequestDispatcher("jsp/unregister-dealer.jsp").forward(request, response);
		} else if (action.equals("Unregister")) {
			String dealerId = request.getParameter("did");
			String reason = request.getParameter("reason");
			boolean success = adminService.unregisterDealer(Integer.parseInt(dealerId), reason);
			if (success)
				request.setAttribute("msg", "unregistered");
			else
				request.setAttribute("msg", "error");
			request.getRequestDispatcher("jsp/unregister-dealer.jsp").forward(request, response);
		} else if (action.equals("viewDealers")) {
			List<Dealer> dealerList = adminService.getDealers();
			request.setAttribute("dealerList", dealerList);
			request.getRequestDispatcher("jsp/view-dealers.jsp").forward(request, response);
		} else if (action.equals("overall")) {
			List<Order> salesList = adminService.getOverallSales();
			request.setAttribute("salesList", salesList);
			request.getRequestDispatcher("jsp/admin-overall-graph.jsp").forward(request, response);
		} else if (action.equals("dealerWise")) {
			request.getRequestDispatcher("jsp/admin-dealer-graph.jsp").forward(request, response);
		} else if (action.equals("dealerName")) {
			int dealerId = Integer.parseInt(request.getParameter("did"));
			List<Order> salesList = adminService.getSalesDataPerDealer(dealerId);
			request.setAttribute("salesList", salesList);
			request.setAttribute("dealer", dealerId);
			request.getRequestDispatcher("jsp/admin-dealer-graph.jsp").forward(request, response);
		}
	}

	public static String generateCSRFToken() {
		String token = UUID.randomUUID().toString();
		return token;
	}

}
