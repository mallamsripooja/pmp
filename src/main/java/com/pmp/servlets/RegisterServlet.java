package com.pmp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.pmp.elements.ContactDetails;
import com.pmp.elements.Dealer;
import com.pmp.services.DealerService;
import com.pmp.services.UserService;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbconnection;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DealerService dealerService = null;

		try {
			dbconnection = ds.getConnection();
			dealerService = new DealerService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (request.getParameter("registerDealerPage") != null) {
			request.getRequestDispatcher("jsp/dealer-registration-page.jsp").forward(request, response);
		} else if (request.getParameter("registerUserPage") != null) {
			request.getRequestDispatcher("jsp/user-registration-page.jsp").forward(request, response);
		} else if (request.getParameter("registerDealer") != null) {
			int result = dealerService.insertDealerDetails(mapDealerDetails(request));
			String message = "";

			if (result != -1)
				message = result + "";
			else
				message = "Error";
			request.setAttribute("message", message);
			request.getRequestDispatcher("jsp/dealer-registration-page.jsp").forward(request, response);
		} else if (request.getParameter("registerUser") != null)
			register(request, response);
		else {
			request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
		}
	}

	private Dealer mapDealerDetails(HttpServletRequest request) {
		Dealer dealer = new Dealer();
		dealer.setName(request.getParameter("firstName") + " " + request.getParameter("lastName"));
		dealer.setCompany(request.getParameter("company"));
		ContactDetails contactDetails = new ContactDetails();
		contactDetails.setContactNumber(request.getParameter("mobile"));
		contactDetails.setAddress(request.getParameter("address"));
		contactDetails.setMailId(request.getParameter("email"));
		dealer.setContactDetails(contactDetails);
		return dealer;
	}

	private void register(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserService userService = null;

		try {
			dbconnection = ds.getConnection();
			userService = new UserService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String address = request.getParameter("address");
		String mobile = request.getParameter("mobile");
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		int result = userService.insertUserDetails(firstName, lastName, address, mobile, email, password, dbconnection);
		String message = "";

		if (result != -1)
			message = result + "";
		else
			message = "Error";
		request.setAttribute("message", message);
		request.getRequestDispatcher("jsp/user-registration-page.jsp").forward(request, response);
	}
}
