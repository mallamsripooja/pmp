package com.pmp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.pmp.elements.ContactDetails;
import com.pmp.elements.Order;
import com.pmp.elements.User;
import com.pmp.services.CategoryService;
import com.pmp.services.DealerService;
import com.pmp.services.ProductService;
import com.pmp.services.UserService;

@WebServlet("/user.do")
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbconnection;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DealerService dealerService = null;
		CategoryService categoryService = null;
		ProductService productService = null;

		try {
			dbconnection = ds.getConnection();
			dealerService = new DealerService(dbconnection);
			categoryService = new CategoryService(dbconnection);
			productService = new ProductService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.setAttribute("dealers", dealerService.getAllDealers());
		request.setAttribute("categories", categoryService.getAllCategories());
		request.setAttribute("products", productService.getAllProducts());
		response.setHeader("X-XSS-Protection", "1; mode=block");
		request.getRequestDispatcher("jsp/user-home-page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DealerService dealerService = null;
		UserService userService = null;
		CategoryService categoryService = null;
		ProductService productService = null;

		try {
			dbconnection = ds.getConnection();
			dealerService = new DealerService(dbconnection);
			userService = new UserService(dbconnection);
			categoryService = new CategoryService(dbconnection);
			productService = new ProductService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String action = request.getParameter("action");
		String storedToken = (String) request.getSession().getAttribute("csrfToken");
		String token = request.getParameter("token");

		if (action != null && action.equals("addToCart")) {
			int userId = Integer.parseInt(request.getParameter("uid"));
			int productId = Integer.parseInt(request.getParameter("pid"));
			userService.insertIntoCart(userId, productId, "cart");
		}

		else if (action != null && action.equals("buy")) {
			int userId = Integer.parseInt(request.getParameter("uid"));
			int productId = Integer.parseInt(request.getParameter("pid"));
			userService.insertIntoCart(userId, productId, "bought");
			String amount = request.getParameter("amount");
			request.setAttribute("amount", amount);
			request.getRequestDispatcher("jsp/checkout.jsp").forward(request, response);

		}
			

			else if (action != null && request.getParameter("action").equals("deleteItem")) {
				int transactionId = Integer.parseInt(request.getParameter("tid"));
				userService.deleteTransaction(transactionId);
			}

			else if (action != null && request.getParameter("action").equals("updateItem")) {
				int transactionId = Integer.parseInt(request.getParameter("tid"));
				int quantity = Integer.parseInt(request.getParameter("qty"));
				userService.updateTransaction(transactionId, quantity);
			}

		if (storedToken.equals(token)) {
			if (request.getParameter("searchProductsSubmit") != null) {
				request.setAttribute("products",
						productService.searchProductsByName(request.getParameter("searchInput")));
				request.setAttribute("dealers", dealerService.getAllDealers());
				response.setHeader("X-XSS-Protection", "1; mode=block");
				request.getRequestDispatcher("jsp/user-product-search-page.jsp").forward(request, response);
			}

			else if (request.getParameter("viewProfileSubmit") != null) {
				int userId = (int) request.getSession(false).getAttribute("userId");
				request.setAttribute("userDetails", userService.getUserByUserId(userId));
				request.getRequestDispatcher("jsp/user-profile-page.jsp").forward(request, response);
			}

			else if (request.getParameter("updateUserDetails") != null) {
				int userId = (int) request.getSession(false).getAttribute("userId");
				userService.updateUserDetails(userId, mapUserDetails(request));
				request.setAttribute("userDetails", userService.getUserByUserId(userId));
				request.getRequestDispatcher("jsp/user-profile-page.jsp").forward(request, response);
			}

			else if (request.getParameter("viewDealersSubmit") != null) {
				request.setAttribute("dealers", dealerService.getAllDealers());
				request.setAttribute("products", productService.getAllProducts());
				request.getRequestDispatcher("jsp/user-view-dealer-page.jsp").forward(request, response);
			}

			else if (request.getParameter("viewCartSubmit") != null) {
				int userId = (int) request.getSession(false).getAttribute("userId");
				List<Order> orders = userService.getAllOrders(userId);
				request.setAttribute("orderList", orders);
				request.getRequestDispatcher("jsp/cart-page.jsp").forward(request, response);
			}
			else if (request.getParameter("checkout") != null) {
				String amount = request.getParameter("amount");
				request.setAttribute("amount", amount);
				request.getRequestDispatcher("jsp/checkout.jsp").forward(request, response);
			}

			else if (action == null) {
				request.setAttribute("dealers", dealerService.getAllDealers());
				request.setAttribute("categories", categoryService.getAllCategories());
				request.setAttribute("products", productService.getAllProducts());
				request.getRequestDispatcher("jsp/user-home-page.jsp").forward(request, response);
			} else {
				request.setAttribute("dealers", dealerService.getAllDealers());
				request.setAttribute("categories", categoryService.getAllCategories());
				request.setAttribute("products", productService.getAllProducts());
				request.getRequestDispatcher("jsp/user-home-page.jsp").forward(request, response);
			}
		}
	}

	private User mapUserDetails(HttpServletRequest request) {
		User user = new User();
		user.setName(request.getParameter("firstName") + " " + request.getParameter("lastName"));
		ContactDetails contactDetails = new ContactDetails();
		contactDetails.setContactNumber(request.getParameter("mobile"));
		contactDetails.setAddress(request.getParameter("address"));
		contactDetails.setMailId(request.getParameter("email"));
		user.setContactDetails(contactDetails);
		return user;
	}

	public static String generateCSRFToken() {
		String token = UUID.randomUUID().toString();
		return token;
	}
}
