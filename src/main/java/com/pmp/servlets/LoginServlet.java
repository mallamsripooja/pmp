package com.pmp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.pmp.services.DealerService;
import com.pmp.services.UserService;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbconnection;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("inside doGet login servlet");
		request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DealerService dealerService = null;
		UserService userService = null;

		try {
			dbconnection = ds.getConnection();
			dealerService = new DealerService(dbconnection);
			userService = new UserService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("inside doPost login servlet");
		System.out.println(request.getParameter("dealerLoginSubmit"));

		if (request.getParameter("dealerLoginSubmit") != null) {
			System.out.println("dealer validation login servlet");
			int userId = Integer.parseInt(request.getParameter("userId"));
			String password = request.getParameter("password");

			boolean isDealerValid = dealerService.isDealerValid(userId, password);

			if (isDealerValid) {
				request.getSession().setAttribute("userId", userId);
				request.getSession().setAttribute("csrfToken", generateCSRFToken());
				String sessionid = request.getSession().getId();
				response.setHeader("Set-Cookie", "JSESSIONID=" + sessionid + ";");
				response.sendRedirect("dealer.do");
			} else {
				String invalidCredentialsAlert="<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">\r\n" + 
						"  <strong>Invalid login credentials!&emsp;</strong> \r\n" + 
						"  <a href=\"#\" data-toggle=\"modal\" data-target=\"#dealerLoginModal\">\r\n" + 
						"	Try again <i class=\"fa fa-repeat\"></i>\r\n" + 
						"  </a>\r\n" + 
						"  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n" + 
						"    <span aria-hidden=\"true\">&times;</span>\r\n" + 
						"  </button>\r\n" + 
						"</div>";
				request.setAttribute("error", invalidCredentialsAlert);
				request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
			}
		}

		else if (request.getParameter("userLoginSubmit") != null) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			String password = request.getParameter("password");

			boolean isUserValid = userService.isUserValid(userId, password);

			if (isUserValid) {
				request.getSession().setAttribute("userId", userId);
				request.getSession().setAttribute("csrfToken", generateCSRFToken());
				String sessionid = request.getSession().getId();
				response.setHeader("Set-Cookie", "JSESSIONID=" + sessionid + ";");
				response.sendRedirect("user.do");
			} else {
				String invalidCredentialsAlert="<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">\r\n" + 
						"  <strong>Invalid login credentials!&emsp;</strong> \r\n" + 
						"  <a href=\"#\" data-toggle=\"modal\" data-target=\"#userLoginModal\">\r\n" + 
						"	Try again <i class=\"fa fa-repeat\"></i>\r\n" + 
						"  </a>\r\n" + 
						"  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n" + 
						"    <span aria-hidden=\"true\">&times;</span>\r\n" + 
						"  </button>\r\n" + 
						"</div>";
				request.setAttribute("error", invalidCredentialsAlert);
				request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
			}
		}

		else if (request.getParameter("registerDealerPage") != null) {
			request.getRequestDispatcher("jsp/dealer-registration-page.jsp").forward(request, response);
		}

		else if (request.getParameter("registerDealerPage") != null) {
			request.getRequestDispatcher("jsp/user-registration-page.jsp").forward(request, response);
		}

		else {
			request.setAttribute("error", "Invalid credientials");
			request.getRequestDispatcher("jsp/home-page.jsp").forward(request, response);
		}
	}

	public static String generateCSRFToken() {
		String token = UUID.randomUUID().toString();
		return token;
	}
}
