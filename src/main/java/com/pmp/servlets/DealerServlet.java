package com.pmp.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

import com.pmp.elements.ContactDetails;
import com.pmp.elements.Dealer;
import com.pmp.elements.Product;
import com.pmp.services.CategoryService;
import com.pmp.services.DealerService;
import com.pmp.services.ProductService;

@WebServlet(urlPatterns = "/dealer.do", initParams = { @WebInitParam(name = "basePath", value = "/") })
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
		maxFileSize = 1024 * 1024 * 10, // 10MB
		maxRequestSize = 1024 * 1024 * 50)
public class DealerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/pmp")
	private DataSource ds;
	Connection dbconnection;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("inside doGet dealer");

		CategoryService categoryService = null;
		ProductService productService = null;

		try {
			dbconnection = ds.getConnection();
			categoryService = new CategoryService(dbconnection);
			productService = new ProductService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		int dealerId = (int) request.getSession(false).getAttribute("userId");
		request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
		request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
		request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("inside doPost dealer");

		DealerService dealerService = null;
		CategoryService categoryService = null;
		ProductService productService = null;

		try {
			dbconnection = ds.getConnection();
			dealerService = new DealerService(dbconnection);
			categoryService = new CategoryService(dbconnection);
			productService = new ProductService(dbconnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (request.getParameter("addProductSubmit") != null) {
			String storedToken = (String) request.getSession().getAttribute("csrfToken");
			String token = request.getParameter("token");

			if (storedToken.equals(token)) {

				if (request.getParameter("addProductSubmit") != null) {
					dealerService.addNewProduct(mapProductDetails(request, categoryService));
					int dealerId = (int) request.getSession(false).getAttribute("userId");
					request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
					request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
					request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
				}

			}
		}

		else if (request.getParameter("updateProductSubmit") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			productService.updateProduct(Integer.parseInt(request.getParameter("productId")),
					mapProductDetails(request, categoryService));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
		}

		else if (request.getParameter("deleteProductSubmit") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			productService.deleteProduct(Integer.parseInt(request.getParameter("productId")));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
		}

		else if (request.getParameter("viewProfileSubmit") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			request.setAttribute("dealerDetails", dealerService.getDealerByDealerId(dealerId));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-profile-page.jsp").forward(request, response);
		}

		else if (request.getParameter("updateDealerDetails") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			dealerService.updateDealerDetails(dealerId, mapDealerDetails(request));
			request.setAttribute("dealerDetails", dealerService.getDealerByDealerId(dealerId));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-profile-page.jsp").forward(request, response);
		}

		else if (request.getParameter("viewReports") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			request.setAttribute("salesList", dealerService.getSalesData(dealerId));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			response.setHeader("X-XSS-Protection", "1; mode=block");
			request.getRequestDispatcher("jsp/dealer-reports-page.jsp").forward(request, response);
		}

		else if (request.getParameter("timeBasedDealerGraph") != null) {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			int productId = Integer.parseInt(request.getParameter("pid"));
			request.setAttribute("salesList", dealerService.getSalesData(dealerId));
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.setAttribute("pid", productId);
			request.setAttribute("productList", dealerService.getAnnualSalesData(dealerId, productId));
			request.getRequestDispatcher("jsp/dealer-reports-page.jsp").forward(request, response);
		}

		else if (request.getParameter("addCategorySubmit") != null) {
			dealerService.addNewCategory(request.getParameter("categoryName"),
					(int) request.getSession(false).getAttribute("userId"));
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
		}

		else {
			int dealerId = (int) request.getSession(false).getAttribute("userId");
			request.setAttribute("categories", categoryService.getAllCategoriesByDealerId(dealerId));
			request.setAttribute("products", productService.getAllProductsByDealerId(dealerId));
			request.getRequestDispatcher("jsp/dealer-home-page.jsp").forward(request, response);
		}
	}

	private Product mapProductDetails(HttpServletRequest request, CategoryService categoryService) {
		Product product = new Product();
		product.setProductName(request.getParameter("productName"));
		product.setProductDescription(request.getParameter("productDescription"));
		product.setDealerId((int) request.getSession(false).getAttribute("userId"));
		product.setCategoryId(categoryService.getCategoryIdByCategoryName(request.getParameter("productCategory")));
		product.setCost(Float.parseFloat(request.getParameter("productCost")));
		product.setStockQuantity(Integer.parseInt(request.getParameter("productQuantity")));
		product.setImageFilePath(uploadImageFile(request));

		return product;
	}

	private String uploadImageFile(HttpServletRequest request) {
		String imageFilePath = null;
		try {
			final String SAVE_DIR = "uploadFiles";
			String fileName = request.getSession().getAttribute("userId").toString();
			String appPath = request.getServletContext().getRealPath("");
			String savePath = appPath + File.separator + SAVE_DIR;

			// creates the save directory if it does not exists
			File fileSaveDir = new File(savePath);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdir();
			}

			Part part = request.getPart("productImage");
			fileName = fileName + "_" + extractFileName(part);
			// refines the fileName in case it is an absolute path
			fileName = new File(fileName).getName();
			part.write(savePath + File.separator + fileName);
			imageFilePath = "../" + SAVE_DIR + "/" + fileName;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return imageFilePath;
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf('"') + 1, s.length() - 1);
			}
		}
		return "";
	}

	private Dealer mapDealerDetails(HttpServletRequest request) {
		Dealer dealer = new Dealer();
		dealer.setName(request.getParameter("firstName") + " " + request.getParameter("lastName"));
		dealer.setCompany(request.getParameter("company"));
		ContactDetails contactDetails = new ContactDetails();
		contactDetails.setContactNumber(request.getParameter("mobile"));
		contactDetails.setAddress(request.getParameter("address"));
		contactDetails.setMailId(request.getParameter("email"));
		dealer.setContactDetails(contactDetails);
		return dealer;
	}

	public static String generateCSRFToken() {
		String token = UUID.randomUUID().toString();
		return token;
	}

}
